/** @type {import('next').NextConfig} */
const nextConfig = {
  // images: {
  //   loader: "custom",
  //   loaderFile: "./app/utils/assets.ts",
  // },
  experimental: {
    appDir: true,
  },
};

module.exports = nextConfig;
