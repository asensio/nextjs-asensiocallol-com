import ISingleList from "../components/SingleList/ISingleList";
import ITechnology from "../components/Technologies/ITechnology";

const educations: ISingleList[] = [
  {
    id: Math.random().toString(),
    role: "Computer Science Student",
    company: "University of Informatics Sciences (UCI)",
    period: "Sept 2005 - July 2011",
    description: [
      "The University of Informatics Sciences (UCI) is a Cuban university specializing in computer science and related fields.",
      "Students at UCI study computer science, software engineering, information technology, and related fields. ",
      "Graduates of UCI obtain a bachelor's degree in Computer Science or a related field. ",
      "The university is known for its emphasis on free and open-source software and its mission to contribute to the development of a more equitable and sustainable world through technology."
    ],
    technologies: [
      "/images/icons/brand/html-5.png",
      "/images/icons/brand/css-3.png",
      "/images/icons/brand/typescript.png",
      "/images/icons/brand/c-sharp.png",
      "/images/icons/brand/java.png",
      "/images/icons/brand/php.png",
      "/images/icons/brand/mysql.png",
      "/images/icons/brand/postgresql.png",
      "/images/icons/brand/git.png",
    ]
  }
];

const experiences: ISingleList[] = [
  {
    id: Math.random().toString(),
    role: "Sr. Frontend Developer",
    company: "Norwegian Cruise Line Holdings Ltd · Contract",
    period: "May 2022 - Present",
    src: "/images/icons/brand/ncl.png",
    description: [
      "Develop and maintain web applications for NCL.",
      "Collaborate with development team to meet business requirements and create visually appealing applications.",
      "Build reusable components and libraries to improve development process.",
      "Write clean and efficient code, debug complex issues, and participate in code reviews.",
      "Stay up-to-date with new technologies and recommend improvements to development process."
    ],
    technologies: [
      "/images/icons/brand/angular.png",
      "/images/icons/brand/html-5.png",
      "/images/icons/brand/css-3.png",
      "/images/icons/brand/typescript.png",
      "/images/icons/brand/npm.png",
      "/images/icons/brand/aws.png",
      "/images/icons/brand/docker.png",
      "/images/icons/brand/github.png",
      "/images/icons/brand/jira.png"
    ]
  },
  {
    id: Math.random().toString(),
    role: "Chief Technology Officer",
    company: "Behavioral Service Network · Consulting",
    period: "Nov 2021 - Feb 2022",
    src: "/images/icons/brand/bsn.png",
    description: [
      "Improved company efficiency by renewing technological infrastructure.",
      "Established optimized workflows for test and production environments.",
      "Implemented continuous integration and continuous development using AWS cloud technology.",
      "Developed user registration system for managing digital contracts of new members.",
      "Created and implemented a robust website with optimal configuration and performance using cloud technology."
    ],
    technologies: [
      "/images/icons/brand/jquery.png",
      "/images/icons/brand/html-5.png",
      "/images/icons/brand/css-3.png",
      "/images/icons/brand/javascript.png",
      "/images/icons/brand/typescript.png",
      "/images/icons/brand/bootstrap.png",
      "/images/icons/brand/npm.png",
      "/images/icons/brand/laravel.png",
      "/images/icons/brand/symfony.png",
      "/images/icons/brand/php.png",
      "/images/icons/brand/mysql.png",
      "/images/icons/brand/redis.png",
      "/images/icons/brand/aws.png",
      "/images/icons/brand/docker.png",
      "/images/icons/brand/git.png",
      "/images/icons/brand/github.png",
      "/images/icons/brand/jira.png",
    ]
  },
  {
    id: Math.random().toString(),
    role: "Sr. Full Stack Engineer",
    company: "Power Wizard · Contract",
    period: "Aug 2020 - May 2022",
    src: "/images/icons/brand/power-wizard.png",
    description: [
      "Solve problems with scalable, efficient, and maintainable solutions.",
      "Develop features from initial concept to production-ready results, including Frontend, Backend, and database design.",
      "Learn and implement new technologies, evolve existing ones, and engage with stakeholders for robust solutions.",
      "Participate in sprint planning and code review, take initiative in improving code for the benefit of the team.",
      "Design and develop scalable, AWS-hosted software solutions across the stack, while communicating technical designs to non-technical audiences."
    ],
    technologies: [
      "/images/icons/brand/angular.png",
      "/images/icons/brand/jquery.png",
      "/images/icons/brand/html-5.png",
      "/images/icons/brand/css-3.png",
      "/images/icons/brand/javascript.png",
      "/images/icons/brand/typescript.png",
      "/images/icons/brand/bootstrap.png",
      "/images/icons/brand/npm.png",
      "/images/icons/brand/laravel.png",
      "/images/icons/brand/php.png",
      "/images/icons/brand/mysql.png",
      "/images/icons/brand/redis.png",
      "/images/icons/brand/aws.png",
      "/images/icons/brand/docker.png",
      "/images/icons/brand/mqtt.png",
      "/images/icons/brand/git.png",
      "/images/icons/brand/jira.png",
    ]
  },
  {
    id: Math.random().toString(),
    role: "Full Stack, API Developer",
    company: "Ubicquia · Contract",
    period: "Apr 2019 - Aug 2020",
    src: "/images/icons/brand/ubicquia.png",
    description: [
      "Develop, design and implement new API endpoints for high-profile customer-facing portal to increase system cohesion and manage code complexity.",
      "Improve product aesthetic and UX resulting in a 72% increase in Customers Happiness Index.",
      "Integrate with third-party services and external APIs for high-volume internal web application to standardize user account management and mitigate code duplicity.",
      "Optimize data storage and reporting queries for high-volume internal web application resulting in a 71% decrease in response time.",
      "Collaborate with developers, designers, stakeholders, and QA teams to deliver highly functional features and recommend business process flow improvements."
    ],
    technologies: [
      "/images/icons/brand/angular.png",
      "/images/icons/brand/jquery.png",
      "/images/icons/brand/html-5.png",
      "/images/icons/brand/css-3.png",
      "/images/icons/brand/javascript.png",
      "/images/icons/brand/typescript.png",
      "/images/icons/brand/bootstrap.png",
      "/images/icons/brand/npm.png",
      "/images/icons/brand/laravel.png",
      "/images/icons/brand/php.png",
      "/images/icons/brand/mysql.png",
      "/images/icons/brand/postgres.png",
      "/images/icons/brand/redis.png",
      "/images/icons/brand/aws.png",
      "/images/icons/brand/docker.png",
      "/images/icons/brand/mqtt.png",
      "/images/icons/brand/git.png",
      "/images/icons/brand/jira.png",
    ]
  },
  {
    id: Math.random().toString(),
    role: "Sr. Web Developer",
    company: "Altimetrik · Contract",
    period: "Aug 2017 - Dec 2018",
    src: "/images/icons/brand/altimetrik.png",
    description: [
      "Maintain newly developed and legacy systems using Java, Angular, and Salesforce APEX languages.",
      "Document solution architecture for critical external web app reducing TTM by 53% and ensuring high code maintainability.",
      "Improve product aesthetic and UX of high-profile internal web application resulting in an 84% increase in sales and product revenue.",
      "Collaborate with developers, designers, and stakeholders to deliver highly functional features and QA teams to define test cases and metrics.",
      "Monitor system performance and troubleshoot issues to ensure optimal system functionality."
    ],
    technologies: [
      "/images/icons/brand/jquery.png",
      "/images/icons/brand/html-5.png",
      "/images/icons/brand/css-3.png",
      "/images/icons/brand/javascript.png",
      "/images/icons/brand/npm.png",
      "/images/icons/brand/java.png",
      "/images/icons/brand/mysql.png",
      "/images/icons/brand/git.png",
    ]
  },
  
  {
    id: Math.random().toString(),
    role: "Full Stack, API Developer",
    company: "National Electronic Group for Tourism · Full Time",
    period: "Nov 2014 - May 2017",
    src: "/images/icons/brand/get.png",
    description: [
      "Implementing Best Development Practices: I successfully led a team of 10 developers in capturing and implementing common coding standards. This initiative resulted in a substantial decrease in code complexity and a significant improvement in the maintainability of shared libraries and components",
      "Designing and Implementing a High-Performing REST API: I played a pivotal role in designing and implementing a REST API for a high-volume internal web application. This API achieved an impressive 99.99% uptime, serving as a critical connectivity channel for distributed functionality. By ensuring system cohesion and effectively managing code complexity, I contributed to delivering a seamless user experience.",
      "Integrating Third-Party Services and External APIs: As part of my involvement in a high-profile internal web application, I successfully integrated third-party services and external APIs. This integration resulted in a notable decrease in the system's complexity while enhancing its functionality and expanding its capabilities.",
      "Performing Rigorous Unit and Load Testing: I conducted comprehensive unit and load testing for a high-profile internal web application. Through these tests, I identified critical performance issues during the development phase. By addressing these issues, I significantly improved the system's stability and scalability, ensuring it could handle increasing workloads effectively.",
      "Ensuring Excellence in Delivery: My portfolio showcases my commitment to excellence by combining my skills in capturing best practices, enforcing coding standards, and conducting thorough testing. The result is a collection of successful projects that demonstrate my ability to deliver efficient, reliable, and scalable solutions."
    ],
    technologies: [
      "/images/icons/brand/angular.png",
      "/images/icons/brand/jquery.png",
      "/images/icons/brand/html-5.png",
      "/images/icons/brand/css-3.png",
      "/images/icons/brand/javascript.png",
      "/images/icons/brand/typescript.png",
      "/images/icons/brand/bootstrap.png",
      "/images/icons/brand/npm.png",
      "/images/icons/brand/laravel.png",
      "/images/icons/brand/php.png",
      "/images/icons/brand/mysql.png",
      "/images/icons/brand/postgres.png",
      "/images/icons/brand/redis.png",
      "/images/icons/brand/aws.png",
      "/images/icons/brand/docker.png",
      "/images/icons/brand/mqtt.png",
      "/images/icons/brand/git.png",
      "/images/icons/brand/jira.png",
    ]
  },
  {
    id: Math.random().toString(),
    role: "Web Developer - Team Leader",
    company: "Vibalco",
    period: "Oct 2013 - Nov 2014",
    src: "/images/icons/brand/vibalco.png",
    description: [
      "Implementing Best Development Practices: I successfully led a team of 10 developers in capturing and implementing common coding standards. This initiative resulted in a substantial decrease in code complexity and a significant improvement in the maintainability of shared libraries and components",
      "Designing and Implementing a High-Performing REST API: I played a pivotal role in designing and implementing a REST API for a high-volume internal web application. This API achieved an impressive 99.99% uptime, serving as a critical connectivity channel for distributed functionality. By ensuring system cohesion and effectively managing code complexity, I contributed to delivering a seamless user experience.",
      "Integrating Third-Party Services and External APIs: As part of my involvement in a high-profile internal web application, I successfully integrated third-party services and external APIs. This integration resulted in a notable decrease in the system's complexity while enhancing its functionality and expanding its capabilities.",
      "Performing Rigorous Unit and Load Testing: I conducted comprehensive unit and load testing for a high-profile internal web application. Through these tests, I identified critical performance issues during the development phase. By addressing these issues, I significantly improved the system's stability and scalability, ensuring it could handle increasing workloads effectively.",
      "Ensuring Excellence in Delivery: My portfolio showcases my commitment to excellence by combining my skills in capturing best practices, enforcing coding standards, and conducting thorough testing. The result is a collection of successful projects that demonstrate my ability to deliver efficient, reliable, and scalable solutions."
    ],
    technologies: [
      "/images/icons/brand/angular.png",
      "/images/icons/brand/jquery.png",
      "/images/icons/brand/html-5.png",
      "/images/icons/brand/css-3.png",
      "/images/icons/brand/javascript.png",
      "/images/icons/brand/typescript.png",
      "/images/icons/brand/bootstrap.png",
      "/images/icons/brand/npm.png",
      "/images/icons/brand/laravel.png",
      "/images/icons/brand/php.png",
      "/images/icons/brand/mysql.png",
      "/images/icons/brand/postgres.png",
      "/images/icons/brand/redis.png",
      "/images/icons/brand/aws.png",
      "/images/icons/brand/docker.png",
      "/images/icons/brand/mqtt.png",
      "/images/icons/brand/git.png",
      "/images/icons/brand/jira.png",
    ]
  },
];

const technologies: ITechnology[] = [
  {
    id: "angular", title: "Angular", href: "https://angular.io/",
    src: "/images/icons/brand/angular.png",
    years: {label: "5", value: "50"},
    category: "Frameworks",
    featured: true
  },
  {
    id: "react", title: "React", href: "https://react.dev/",
    src: "/images/icons/brand/reactjs.png",
    years: {label: "2", value: "20"},
    category: "Toolkits",
    featured: true
  },
  {
    id: "vuejs", title: "VueJS", href: "https://vuejs.org/",
    src: "/images/icons/brand/view.png",
    years: {label: "2", value: "20"},
    category: "Toolkits",
    featured: false
  },
  {
    id: "jquery", title: "jQuery", href: "https://jquery.com/",
    src: "/images/icons/brand/jquery.png",
    years: {label: "10", value: "85"},
    category: "Toolkits",
    featured: false
  },
  {
    id: "html5", title: "HTML5", href: "https://www.w3schools.com/html/",
    src: "/images/icons/brand/html-5.png",
    years: {label: "12", value: "95"},
    category: "Front End",
    featured: false
  },
  {
    id: "css3", title: "CSS3", href: "https://www.w3schools.com/Css/",
    src: "/images/icons/brand/css-3.png",
    years: {label: "12", value: "95"},
    category: "Front End",
    featured: false
  },
  {
    id: "javascript", title: "JavaScript", href: "https://www.w3schools.com/javascript/",
    src: "/images/icons/brand/javascript.png",
    years: {label: "12", value: "95"},
    category: "Front End",
    featured: false
  },
  {
    id: "typescript", title: "TypeScript", href: "https://www.w3schools.com/typescript/",
    src: "/images/icons/brand/typescript.png",
    years: {label: "5", value: "50"},
    category: "Front End",
    featured: false
  },
  {
    id: "bootstrap", title: "Bootstrap", href: "https://getbootstrap.com/",
    src: "/images/icons/brand/bootstrap.png",
    years: {label: "10", value: "85"},
    category: "Toolkits",
    featured: false
  },
  {
    id: "npm", title: "NPM", href: "https://www.npmjs.com",
    src: "/images/icons/brand/npm.png",
    years: {label: "7", value: "70"},
    category: "Toolkits",
    featured: false
  },
  {
    id: "laravel", title: "Laravel", href: "https://laravel.com/",
    src: "/images/icons/brand/laravel.png",
    years: {label: "7", value: "70"},
    category: "Frameworks",
    featured: true
  },
  {
    id: "symfony", title: "Symfony", href: "https://symfony.com",
    src: "/images/icons/brand/symfony.png",
    years: {label: "7", value: "70"},
    category: "Frameworks",
    featured: false
  },
  {
    id: "ghost", title: "Ghost", href: "https://ghost.org/",
    src: "/images/icons/brand/ghost.png",
    years: {label: "7", value: "70"},
    category: "Frameworks",
    featured: false
  },
  {
    id: "php", title: "PHP", href: "https://www.php.net/",
    src: "/images/icons/brand/php.png",
    years: {label: "12", value: "95"},
    category: "Back End",
    featured: false
  },
  {
    id: "java", title: "Java", href: "https://www.java.com/en/",
    src: "/images/icons/brand/java.png",
    years: {label: "5", value: "50"},
    category: "Back End",
    featured: false
  },
  {
    id: "c#", title: "C#", href: "https://learn.microsoft.com/en-us/dotnet/csharp/",
    src: "/images/icons/brand/c-sharp.png",
    years: {label: "5", value: "50"},
    category: "Back End",
    featured: false
  },
  {
    id: "mysql", title: "MySQL", href: "https://www.mysql.com",
    src: "/images/icons/brand/mysql.png",
    years: {label: "12", value: "95"},
    category: "Database",
    featured: false
  },
  {
    id: "postgresql", title: "PostgreSQL", href: "https://www.postgresql.org",
    src: "/images/icons/brand/sqlite.png",
    years: {label: "12", value: "95"},
    category: "Database",
    featured: false
  },
  {
    id: "sqlite", title: "Sqlite", href: "https://www.sqlite.org",
    src: "/images/icons/brand/sqlite.png",
    years: {label: "12", value: "95"},
    category: "Database",
    featured: false
  },
  {
    id: "redis", title: "Redis", href: "https://redis.io",
    src: "/images/icons/brand/redis.png",
    years: {label: "7", value: "70"},
    category: "Database",
    featured: false
  },
  {
    id: "aws", title: "AWS", href: "https://aws.amazon.com",
    src: "/images/icons/brand/aws.png",
    years: {label: "5", value: "50"},
    category: "Cloud",
    featured: true
  },
  {
    id: "docker", title: "Docker", href: "https://www.docker.com",
    src: "/images/icons/brand/docker.png",
    years: {label: "5", value: "50"},
    category: "Cloud",
    featured: false
  },
  {
    id: "mqtt", title: "MQTT", href: "https://mqtt.org",
    src: "/images/icons/brand/mqtt.png",
    years: {label: "2", value: "20"},
    category: "Cloud",
    featured: false
  },
  {
    id: "git", title: "Git", href: "https://git-scm.com",
    src: "/images/icons/brand/git.png",
    years: {label: "10", value: "80"},
    category: "Control Version",
    featured: false
  },
  {
    id: "github", title: "GitHub", href: "https://github.com",
    src: "/images/icons/brand/github.png",
    years: {label: "10", value: "80"},
    category: "Control Version",
    featured: false
  },
  {
    id: "gitlab", title: "GitLab", href: "https://gitlab.com",
    src: "/images/icons/brand/gitlab.png",
    years: {label: "10", value: "80"},
    category: "Control Version",
    featured: false
  },
  {
    id: "jira", title: "Jira", href: "https://jira.atlassian.com",
    src: "/images/icons/brand/jira.png",
    years: {label: "5", value: "50"},
    category: "Project Management",
    featured: false
  },
];

const technologies_categories = [
  "Front End",
  "Back End",
  "Frameworks",
  "Toolkits",
  "Database",
  "Cloud",
  "Control Version",
  "Project Management"
];

const references = [
  {
    name: "Hector A. Almaguer",
    role: "Chief Technology Officer",
    src: "/images/references/hector.png",
    href: "www.blindster.com",
    description: "It is very rare to come across standout talent like Abelardo. I have had\n" +
      "the pleasure of knowing him for almost 12 years now, during which we worked together in several\n" +
      "assignments. Above all, I was very impressed with Abelardo’s ability to meet overarching goals and\n" +
      "tight deadlines that sometimes required him to learn new technologies on the fly. Those set of\n" +
      "skills often take years to develop among Software Professionals. Always maintaining a positive\n" +
      "attitude and mindset, Abelardo personality can easily integrate and blend well with any team,\n" +
      "while always exuding amazing work ethic and commitment. As an individual contributor or a leader,\n" +
      "Abelardo earns my highest recommendation for any new role he pursues!"
  },
  {
    name: "Marcos Ortiz Valmaseda",
    role: "Data Engineer at X-Team (working for Riot Games)",
    src: "/images/references/marcos.png",
    href: "x-team.com",
    description: "Abelardo is one of the best Web Developers that I´ve ever worked with. " +
      "When we work together at GET, he always tried to make his best work possible in every assignment, but always he had time to help to his workers (like myself). " +
      "He is a very good Project Manager too, and a great leader, always thinking on the best things for the group. " +
      "Any company thinking to hire Abelardo, will obtain a hard work guy with an incredible sense of humor and responsibility. " +
      "He always will make you to smile with his crazy jokes, but when there is a deadline in place, you have to work very hard to catch him."
  },
]

const data = {
  experiences,
  technologies,
  educations,
  technologies_categories,
  references
}
export default data;
