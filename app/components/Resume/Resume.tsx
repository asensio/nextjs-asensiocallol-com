"use client";
import React, {useState} from "react";
import {Tabs, Tab} from 'react-bootstrap';
import ProfessionalSkills from "../ProfessionalSkills/ProfessionalSkills";
import Experience from "../Experience/Experience";
import Education from "../Education/Education";
import Languages from "../Languages/Languages";

export default function Resume() {
  const [key, setKey] = useState('experience');
  return (
    <div className="rn-resume-area rn-section-gap section-separator" id="resume">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="section-title text-center">
              <span className="subtitle">12+ Years of Experience</span>
              <h2 className="title">My Resume</h2>
            </div>
          </div>
        </div>
        <div className="row mt--45">
          <div className="col-lg-12">
            <Tabs
              id="controlled-tab-example"
              activeKey={key}
              onSelect={(k) => setKey(k ?? "experience")}
              className="rn-nav-list nav nav-tabs"
            >
              <Tab eventKey="experience" title="Experience">
                <Experience/>
              </Tab>
              
              <Tab eventKey="professional" title="Professional Skills">
                <ProfessionalSkills/>
              </Tab>
              
              <Tab eventKey="education" title="Education">
                <Education/>
              </Tab>
              
              <Tab eventKey="languages" title="Languages">
                <Languages/>
              </Tab>
            
            </Tabs>
          </div>
        </div>
      </div>
    </div>
  );
}
