import Image from "next/image";

export default function Portfolio(){
  return (
    <div className="rn-portfolio-area rn-section-gap section-separator" id="portfolio">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="section-title text-center">
              <span className="subtitle">Visit my portfolio and keep your feedback</span>
              <h2 className="title">My Portfolio</h2>
            </div>
          </div>
        </div>
      
        <div className="row row--25 mt--10 mt_md--10 mt_sm--10">
          
          <div data-aos="fade-up" data-aos-delay="300" data-aos-once="true"
               className="col-lg-6 col-xl-4 col-md-6 col-12 mt--50 mt_md--30 mt_sm--30 aos-init aos-animate">
            <div className="rn-portfolio" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
              <div className="inner">
                <div className="thumbnail">
                  <a href="javascript:void(0)">
                    <Image
                      src="/images/portfolio/portfolio-02.jpg"
                      alt="Personal Portfolio Images"
                      width={400}
                      height={400}
                    />
                  </a>
                </div>
                <div className="content">
                  <div className="category-info">
                    <div className="category-list">
                      <span className="text-primary">Application</span>
                    </div>
                    <div className="meta">
                                            <span>
                                              <a href="javascript:void(0)"><i className="feather-heart"></i></a>
                                        750</span>
                    </div>
                  </div>
                  <h4 className="title"><a href="javascript:void(0)">Mobile app landing design &amp; app
                    maintain<i className="feather-arrow-up-right"></i></a></h4>
                </div>
              </div>
            </div>
          </div>
        
        </div>
      </div>
    </div>
  )
}
