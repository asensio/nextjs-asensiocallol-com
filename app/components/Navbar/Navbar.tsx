import React from 'react';
import Link from "next/link";

const links = [
  {href:"", text: "home"},
  {href:"#expertise", text: "expertise"},
  // {href:"#portfolio", text: "portfolio"},
  {href:"#resume", text: "resume"},
  {href:"#references", text: "References"},
  // {href:"#contacts", text: "contacts"},
];

const Navbar = () => {
  return (
    <nav id="sideNav" className="mainmenu-nav  ">
      <ul className="primary-menu nav nav-pills">
        {links.map((link) => (
          <li key={link.href} className="nav-item">
            <Link className="nav-link" href={link.href}>
              {link.text}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Navbar;
