import React from 'react';
import data from "../../database/data";

export default function ProfessionalSkills() {
  return (
    <div className="personal-experience-inner mt--40">
      <div className="row row--40">
        {data?.technologies_categories?.map((category, key) => (
          <div key={key} className="col-lg-6 col-md-6 col-12 mb--70">
            <div className="progress-wrapper">
              <div className="content">
                <span className="subtitle">Years of experience</span>
                <h4 className="maintitle">{category}</h4>
                {data?.technologies?.map((tech, key) => {
                  if (tech.category === category) {
                    return (
                      <div key={key} className="progress-charts">
                        <h6 className="heading heading-h6">
                          <a href={tech?.href} target="_blank">{tech?.title}</a>
                        </h6>
                        <div className="progress">
                          <div className="progress-bar wow fadeInLeft" data-wow-duration="0.5s" data-wow-delay=".3s"
                               role="progressbar" style={{width: `${tech?.years?.value}%`}}>
                            <span className="percent-label">
                              {tech?.years?.label}
                            </span>
                          </div>
                        </div>
                      </div>
                    )
                  }
                })}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
