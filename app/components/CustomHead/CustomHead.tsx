import Head from "next/head";
export default  function CustomHead ({
  title = "Abelardo Asensio Callol",
  description = "With 12+ years of professional experience as a software engineer, I believe my qualifications will match your requirements. I have had the opportunity to function in a variety of positions within various teams, making it easy to adapt to new challenges and concepts. I am confident that I can be a valuable asset to your organization while enabling me to develop and grow professionally.Thank you very much for your time and consideration. I look forward to discussing this opportunity with you.",
  keywords = "",
  url = "",
  language = "en",
}) {
    return  (
      <Head>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <meta name="theme-color" content="#118b92" />
      
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta name="keywords" content={keywords} />
          <meta name="author" content={title} />
      
          <meta property="og:site_name" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:url" content={`${language}/${url}`} />
          <meta property="og:image" content="/images/logo/logo.png" />
          <meta property="og:description" content={description} />
          <meta property="og:type" content="website" />
      
          <meta name="twitter:title" content={title} />
          <meta name="twitter:description" content={description} />
          <meta name="twitter:image" content="/images/logo/logo.png" />
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="theme-color" content="#118b92" />
      
          <link rel="alternate" href={`${url}`} hrefLang="nl" />
          <link rel="alternate" href={`${url}`} hrefLang="en" />
          <link rel="alternate" href={`${url}`} hrefLang="fr" />
      
          <link
            rel="icon"
            type="image/png"
            href="/images/logo/logo.png"
            sizes="16x16"
          />
          <link
            rel="icon"
            type="image/png"
            href="/images/logo/logo.png"
            sizes="32x32"
          />
          <link rel="apple-touch-icon" href="/images/logo/logo.png" />
          <link rel="apple-touch-icon" sizes="180x180" href="/" />
          <link rel="mask-icon" href="/" color="#d04819" />
          <link rel="shortcut icon" href="/images/logo/logo.png" />
      
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css"
            integrity="sha512-1cK78a1o+ht2JcaW6g8OXYwqpev9+6GqOkz9xmBN9iUUhIndKtxwILGWYOSibOKjLsEdjyjZvYDq/cZwNeak0w=="
            crossOrigin="anonymous"
            referrerPolicy="no-referrer"
          />
      </Head>
    );
};
