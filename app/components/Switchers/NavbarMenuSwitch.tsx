"use client";
import React, {useState, useEffect} from 'react';

export default function NavbarMenuSwitch() {
  const [menu, setMenu] = useState('');
  
  function toggleTheme() {
    if (menu === 'menu-open') {
      setMenu('');
      document.getElementById("popup-mobile-menu")?.classList.remove('white-version');
    } else {
      setMenu('menu-open');
      document.body.classList.add('white-version');
    }
  }
  
  useEffect(() => {
    const systemTheme = window.matchMedia('(prefers-color-scheme: dark)').matches
      ? 'dark'
      : 'menu-open';
    setMenu(systemTheme);
    if (systemTheme === 'dark') {
      document.body.classList.remove('white-version');
    } else {
      document.body.classList.add('white-version');
    }
  }, []);
  
  return (
    <div onClick={toggleTheme} className="d-block d-xl-none mr--25">
      <svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" fill="currentColor"
           className="bi bi-list" viewBox="0 0 16 16">
        <path fillRule="evenodd"
              d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
      </svg>
    </div>
  );
}
