"use client";
import React, {useState, useEffect} from 'react'

export default function ThemeSwitch() {
  const [theme, setTheme] = useState('light')
  
  function toggleTheme() {
    if (theme === 'light') {
      setTheme('dark')
      document.body.classList.remove('white-version')
    } else {
      setTheme('light')
      document.body.classList.add('white-version')
    }
  }
  
  useEffect(() => {
    const systemTheme = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'
    setTheme(systemTheme)
    if (systemTheme === 'dark') {
      document.body.classList.remove('white-version')
    } else {
      document.body.classList.add('white-version')
    }
  }, [])
  
  return (
    <label className="switch">
      <input onClick={toggleTheme} type="checkbox"/>
      <span className="slider"></span>
    </label>
  )
}
