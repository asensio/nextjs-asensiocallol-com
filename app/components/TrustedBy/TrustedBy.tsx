"use client";
import React, { Component } from "react";
import Slider from "react-slick";
import Image from "next/image";
import data from "../../database/data";

function SampleNextArrow(props: any) {
  return (<></>);
}

function SamplePrevArrow(props: any) {
  return (<></>);
}

export default class TrustedBy extends Component<any, any> {
  
  constructor(props: any) {
    super(props);
  }
  
  render() {
    const settings = {
      dots: false,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      speed: 2000,
      autoplaySpeed: 2000,
      cssEase: "linear",
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />
    };
    return (
      <div>
        <div className="rn-service-area rn-section-gap section-separator" id="expertise">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="section-title text-center">
                  <span className="subtitle">Trusted By</span>
                  <h2 className="title">COMPANIES</h2>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12 pt--70">
                <Slider {...settings}>
                  {data.experiences?.map((experience) => (
                    <div key={experience.role}>
                      {experience.src && (
                        <Image
                          src={experience.src}
                          width={335}
                          height={252}
                          alt={experience.role}
                        />
                      )}
                    </div>
                  ))}
                </Slider>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
