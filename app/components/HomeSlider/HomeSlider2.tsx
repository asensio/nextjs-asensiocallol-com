"use client";
import Image from "next/image";
import aboutPicture from "../../../public/images/about/about.png";
import { useState } from "react";
import Typed from "react-typed";
import FindMeOnSocialMedia from "../FindMeOnSocialMedia/FindMeOnSocialMedia";

interface HomeSliderProps {
  title?: string;
  subtitle?: string;
}

const HomeSlider = (props: HomeSliderProps) => {
  const [data, setData] = useState({
    title: "Abelardo Asensio",
    subtitle: "Sr. Software Engineer",
    description:
      "Abelardo Asensio is a highly skilled Sr. Software Engineer with over 12 years of experience in designing and implementing innovative software solutions for various industries. With expertise in a wide range of technologies and coding languages, Abelardo is well-equipped to take on complex software development projects and deliver exceptional results.",
    tags: [
      "Sr. Software Engineer",
      "FullStack Developer",
      "Professional Coder",
      "Tech Enthusiast",
      "AI Enthusiast",
    ],
  });

  return (
    <div id="home" className="rn-slider-area">
      <div className="slide slider-style-2">
        <div className="container">
          <div className="row align-items-center row--30">
            <div className="col-lg-5">
              <div className="thumbnail style-2">
                <div className="inner ">
                  <Image
                    className="w-100"
                    src={aboutPicture}
                    width="436"
                    height="580"
                    alt="Icons Images"
                  />
                </div>
              </div>
            </div>

            <div className="col-lg-7 mt_md--40 mt_sm--40">
              <div className="content">
                <div className="inner">
                  <h1 className="title">
                    Hi, I’m <span>{data.title}</span>
                    <br />
                    <span className="header-caption" id="page-top">
                      <span className="cd-headline clip is-full-width">
                        <span className="mr-5">a </span>
                        <Typed
                          className="cd-words-wrapper"
                          strings={data.tags}
                          typeSpeed={40}
                          backSpeed={50}
                          loop
                        ></Typed>
                      </span>
                    </span>
                  </h1>

                  <div>
                    <p className="description">{data.description}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <FindMeOnSocialMedia />
        </div>
      </div>
    </div>
  );
};
export default HomeSlider;
