"use client"
import {useState} from "react";

export default function Expertise() {
  
  const [expertises, setExpertises] = useState([
    {
       title: 'UX/UI Experience ',
      description: 'Applying interactive and visual design principles on websites and mobile applications for a positive and cohesive user experience.'
    },
    {
      title: 'Clean code',
      description: 'Based on best practices and technical solutions to make projects scalable, maintainable, documentable, and efficient.'
    },
    {
      title: 'Integration',
      description: 'Microservices, RESTful APIs, integrated back-end systems and real-time insights using cutting edge technologies and frameworks.'
    },
    {
      title: 'Performance',
      description: 'Ability to work independently bringing ideas to life at record speed.'
    },
  ]);
  return (
    <>
      <div className="rn-service-area rn-section-gap section-separator" id="expertise">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-title text-left">
                <span className="subtitle">Expertise</span>
                <h2 className="title">What I Do</h2>
              </div>
            </div>
          </div>
          <div className="row row--25 mt_md--10 mt_sm--10">
            <div key={expertises[0].title} className="col-lg-6 col-md-6 col-sm-12 col-12 mt--50 mt_md--30 mt_sm--30">
              <div className="rn-service">
                <div className="inner">
                  <div className="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         className="bi bi-rulers" viewBox="0 0 16 16">
                      <path
                        d="M1 0a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h5v-1H2v-1h4v-1H4v-1h2v-1H2v-1h4V9H4V8h2V7H2V6h4V2h1v4h1V4h1v2h1V2h1v4h1V4h1v2h1V2h1v4h1V1a1 1 0 0 0-1-1H1z"/>
                    </svg>
                  </div>
                  <div className="content">
                    <h4 className="title">
                      {expertises[0].title}
                    </h4>
                    <p className="description">
                      {expertises[0].description}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            
            <div key={expertises[1].title} className="col-lg-6 col-md-6 col-sm-12 col-12 mt--50 mt_md--30 mt_sm--30">
              <div className="rn-service">
                <div className="inner">
                  <div className="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         className="bi bi-braces-asterisk" viewBox="0 0 16 16">
                      <path fillRule="evenodd"
                            d="M1.114 8.063V7.9c1.005-.102 1.497-.615 1.497-1.6V4.503c0-1.094.39-1.538 1.354-1.538h.273V2h-.376C2.25 2 1.49 2.759 1.49 4.352v1.524c0 1.094-.376 1.456-1.49 1.456v1.299c1.114 0 1.49.362 1.49 1.456v1.524c0 1.593.759 2.352 2.372 2.352h.376v-.964h-.273c-.964 0-1.354-.444-1.354-1.538V9.663c0-.984-.492-1.497-1.497-1.6ZM14.886 7.9v.164c-1.005.103-1.497.616-1.497 1.6v1.798c0 1.094-.39 1.538-1.354 1.538h-.273v.964h.376c1.613 0 2.372-.759 2.372-2.352v-1.524c0-1.094.376-1.456 1.49-1.456v-1.3c-1.114 0-1.49-.362-1.49-1.456V4.352C14.51 2.759 13.75 2 12.138 2h-.376v.964h.273c.964 0 1.354.444 1.354 1.538V6.3c0 .984.492 1.497 1.497 1.6ZM7.5 11.5V9.207l-1.621 1.621-.707-.707L6.792 8.5H4.5v-1h2.293L5.172 5.879l.707-.707L7.5 6.792V4.5h1v2.293l1.621-1.621.707.707L9.208 7.5H11.5v1H9.207l1.621 1.621-.707.707L8.5 9.208V11.5h-1Z"/>
                    </svg>
                  </div>
                  <div className="content">
                    <h4 className="title">
                      {expertises[1].title}
                    </h4>
                    <p className="description">
                      {expertises[1].description}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            
            <div key={expertises[2].title} className="col-lg-6 col-md-6 col-sm-12 col-12 mt--50 mt_md--30 mt_sm--30">
              <div className="rn-service">
                <div className="inner">
                  <div className="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         className="bi bi-git" viewBox="0 0 16 16">
                      <path
                        d="M15.698 7.287 8.712.302a1.03 1.03 0 0 0-1.457 0l-1.45 1.45 1.84 1.84a1.223 1.223 0 0 1 1.55 1.56l1.773 1.774a1.224 1.224 0 0 1 1.267 2.025 1.226 1.226 0 0 1-2.002-1.334L8.58 5.963v4.353a1.226 1.226 0 1 1-1.008-.036V5.887a1.226 1.226 0 0 1-.666-1.608L5.093 2.465l-4.79 4.79a1.03 1.03 0 0 0 0 1.457l6.986 6.986a1.03 1.03 0 0 0 1.457 0l6.953-6.953a1.031 1.031 0 0 0 0-1.457"/>
                    </svg>
                  </div>
                  <div className="content">
                    <h4 className="title">
                      {expertises[2].title}
                    </h4>
                    <p className="description">
                      {expertises[2].description}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            
            
            <div key={expertises[3].title} className="col-lg-6 col-md-6 col-sm-12 col-12 mt--50 mt_md--30 mt_sm--30">
              <div className="rn-service">
                <div className="inner">
                  <div className="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         className="bi bi-lightning" viewBox="0 0 16 16">
                      <path
                        d="M5.52.359A.5.5 0 0 1 6 0h4a.5.5 0 0 1 .474.658L8.694 6H12.5a.5.5 0 0 1 .395.807l-7 9a.5.5 0 0 1-.873-.454L6.823 9.5H3.5a.5.5 0 0 1-.48-.641l2.5-8.5zM6.374 1 4.168 8.5H7.5a.5.5 0 0 1 .478.647L6.78 13.04 11.478 7H8a.5.5 0 0 1-.474-.658L9.306 1H6.374z"/>
                    </svg>
                  </div>
                  <div className="content">
                    <h4 className="title">
                      {expertises[3].title}
                    </h4>
                    <p className="description">
                      {expertises[3].description}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </>
  );
}
