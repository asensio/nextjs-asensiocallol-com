import React from 'react';
import data from "../../database/data";
import SingleList from "../SingleList/SingleList";

export default function Experience() {
  
  return (
    <div className="personal-experience-inner mt--40">
      <div className="row">
        <div className="col-lg-6 col-sm-12 mb--25">
          <div className="content">
            <span className="subtitle">2020 - PRESENT</span>
            <h4 className="maintitle">Job Experience</h4>
            <div className="experience-list">
              {data?.experiences?.slice(0, 3)?.map((experience) => (
                <SingleList
                  key={`${experience?.id}-${experience?.role}`}
                  single={experience}/>
              ))}
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-sm-12">
          <div className="content">
            <span className="subtitle">2013 - 2020</span>
            <h4 className="maintitle">Job Experience</h4>
            <div className="experience-list">
              {data?.experiences?.slice(3)?.map((experience) => (
                <SingleList
                  key={`${experience?.id}-${experience?.role}`}
                  single={experience}/>
              ))}
            </div>
          </div>
        </div>
      
      </div>
    </div>
  );
}
