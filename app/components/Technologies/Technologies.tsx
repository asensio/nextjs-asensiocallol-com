import React from "react";
import data from "../../database/data";
import Image from "next/image";

const Technologies = ({list}: { list: string[] }) => {
  const technologies = data.technologies.filter(tech => list.includes(tech.src));
  return (
    <div className="slide p-4">
      <div className="skill-share-inner">
        <span className="title">Technologies</span>
        <ul className="skill-share d-flex liststyle">
          {technologies?.map((tech) => (
            <li key={tech.id}>
              <a href={tech.href} target="_blank">
                <Image src={tech.src} alt={tech.title} width={100} height={100}/>
              </a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default Technologies;
