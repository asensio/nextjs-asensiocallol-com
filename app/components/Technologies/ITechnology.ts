interface ITechnology {
  id: string,
  title: string,
  href: string,
  src: string,
  years: { label: string, value: string },
  category: string,
  featured: boolean
}

export default ITechnology;
