import React from 'react';
import data from "../../database/data";
import SingleList from "../SingleList/SingleList";

export default function Education() {
  const {educations} = data;
  
  
  return (
    <div className="personal-experience-inner mt--40">
      <div className="row">
        <div className="col-lg-12 col-md-12 col-12">
          <div className="content">
            <span className="subtitle">2005 - 2011</span>
            <h4 className="maintitle">Education</h4>
            <div className="experience-list">
              {educations?.map((education) => (
                <SingleList key={`${education?.id}-${education?.role}`} single={education}/>
              ))}
            </div>
          </div>
        </div>
      
      </div>
    </div>
  );
}
