import React from 'react';

export default function Languages() {
  
  return (
    <div className="personal-experience-inner mt--40">
      <div className="row">
        <div className="col-6">
          <div className="content">
            <div className="experience-list">
                <div className="resume-single-list">
                  <div className="inner">
                    <div className="heading">
                      <div className="title">
                        <h4>Spanish</h4>
                        <span>Native Language</span>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            
          </div>
        </div>
        <div className="col-6">
          <div className="content">
            <div className="experience-list">
              <div className="resume-single-list">
                <div className="inner">
                  <div className="heading">
                    <div className="title">
                      <h4>English</h4>
                      <span>Professional Level</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      
      </div>
    </div>
  );
}
