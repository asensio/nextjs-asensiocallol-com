"use client";
import React, {Component} from "react";
import Slider from "react-slick";
import Image from "next/image";
import data from "../../database/data";

function SampleNextArrow(props: any) {
  const {style, onClick} = props;
  return (
    <button className={`slide-arrow slick-arrow`} style={{...style}} onClick={onClick}>
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left"
           viewBox="0 0 16 16">
        <path fillRule="evenodd"
              d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
      </svg>
    </button>
  );
}

function SamplePrevArrow(props: any) {
  const {style, onClick} = props;
  return (
    <button className={`slide-arrow slick-arrow`} style={{...style}} onClick={onClick}>
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-right"
           viewBox="0 0 16 16">
        <path fillRule="evenodd"
              d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
      </svg>
    </button>
  );
}

export default class References extends Component<any, any> {
  
  constructor(props: any) {
    super(props);
  }
  
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      nextArrow: <SampleNextArrow/>,
      prevArrow: <SamplePrevArrow/>
    };
    
    return (
      <div className="rn-testimonial-area rn-section-gap section-separator" id="references">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-title text-center">
                <span className="subtitle">What other software engineers say about me</span>
                <h2 className="title">References</h2>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="container">
                <div className="testimonial-activation testimonial-pb mb--30">
                  <Slider {...settings}>
                    {data?.references?.map(reference => (
                      <div key={reference?.name}>
                        <div className="testimonial mt--50 mt_md--40 mt_sm--40">
                          <div className="inner">
                            <div className="card-info">
                              <div className="card-thumbnail">
                                <Image src={reference?.src} width={335} height={252} alt={reference?.name}/>
                              </div>
                              <div className="card-content">
                                <span className="subtitle mt--10">{reference?.role}</span>
                                <h3 className="title">{reference?.name}</h3>
                                <p className="discription">
                                  <a href={`https://${reference?.href}`} target="_blank"
                                     className="text-color-primary fs-5">{reference?.href}</a>
                                </p>
                              </div>
                            </div>
                            <div className="card-description">
                              <div className="title-area">
                                <div className="title-info">
                                  <h3 className="title">{reference?.name}</h3>
                                  <span className="date">{reference?.role}</span>
                                </div>
                                <div className="rating">
                                  {[...Array(5)].map((_, index) => (
                                    <Image key={index} width={10} height={10} src="/images/icons/rating.png"
                                           alt="rating-image"/>
                                  ))}
                                </div>
                              </div>
                              <div className="seperator"></div>
                              <p className="discription">
                                {reference?.description}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </Slider>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
