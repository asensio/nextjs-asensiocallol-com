interface ISingleList {
  id: string;
  role: string;
  company: string;
  period?: string;
  src?: string;
  description: string[];
  technologies: string[];
}

export default ISingleList;
