import React from 'react';
import Technologies from '../Technologies/Technologies';
import ISingleList from "./ISingleList";

const SingleList: React.FC<{ single: ISingleList }> = ({single}) => (
  <div key={`${single.id}-${single.role}`} className="resume-single-list">
    <div className="inner">
      <div className="heading">
        <div className="title">
          <h4>{single.role}</h4>
          <span>{single.company}</span>
        </div>
        <div className="date-of-time">
          <span>{single.period}</span>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <ul>
            {single.description?.map((desc) => (
              <li key={desc}>
                <span className="text-black">{desc}</span>
              </li>
            ))}
          </ul>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <Technologies list={single?.technologies}/>
        </div>
      </div>
    </div>
  </div>
);

export default SingleList;
