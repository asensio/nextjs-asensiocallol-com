"use client";
import {Component} from "react";
import Image from "next/image";
import Link from "next/link";
import logo from "../../../public/images/logo/logo.png";

export default class Footer extends Component<any, any> {
  render() {
    return (
      <div className="rn-footer-area rn-section-gap section-separator">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="footer-area text-center">
                
                <div className="logo">
                  <Link href="/">
                    <Image src={logo} width="130" height="70" alt="logo"/>
                  </Link>
                </div>
                
                <p className="description mt--30">
                  2019 © All rights reserved by Abelardo Asensio
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
