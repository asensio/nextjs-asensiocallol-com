import Image from "next/image";
import React from "react";
import logo from "../../../public/images/logo/logo.png";
import Navbar from "../Navbar/Navbar";
import ThemeSwitch from "../Switchers/ThemeSwitch";

function Header() {
  return (
    <header className="rn-header haeder-default black-logo-version header--fixed header--sticky">
      <div className="header-wrapper rn-popup-mobile-menu m--0 row align-items-center">
        <div className="col-lg-2 col-6">
          <div className="header-left">
            <div className="logo">
              <a href="/">
                <Image src={logo} width="130" height="70" alt="logo"/>
              </a>
            </div>
          </div>
        </div>
        <div className="col-lg-10 col-6">
          <div className="header-center">
            <Navbar/>
            <div className="header-right">
              <ThemeSwitch/>
              
              <div className="close-menu d-block">
                <span className="closeTrigger"></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
