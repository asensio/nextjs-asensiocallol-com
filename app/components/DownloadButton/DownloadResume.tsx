"use client";
import React from "react";

class DownloadResume extends React.Component<any, any> {
  componentDidMount() {
    //this.handleDownload();
  }
  
  // handleDownload = async () => {
  //   const filePath = "/resume/2023/abelardo-asensio-callol-sr-software-engineer.pdf";
  //   if (typeof window !== "undefined") {
  //     window.open(filePath, "_blank");
  //   }
  // };
  
  render() {
    return (
      <a className="rn-btn mr--20 mt--15" href = "/resume/2023/abelardo-asensio-callol-sr-software-engineer.pdf" download>
        <span>DOWNLOAD RESUME</span>
      </a>
    );
  }
}

export default DownloadResume;
