import React from 'react';
import data from "../../database/data";
import Image from "next/image";

export default function BestSkillOn() {
  return (
    <div className="skill-share-inner">
      <span className="title">best skill on</span>
      <ul className="skill-share d-flex liststyle">
        {data?.technologies?.map((tech, key) => {
          if (tech?.featured) {
            return (
              <li key={`${key}-${tech.id}`}>
              <a href={tech?.href} target="_blank">
                <Image src={tech?.src} alt={tech?.title} width={100} height={100}/>
              </a>
            </li>
            );
          }
        })}
      </ul>
    </div>
  );
}
