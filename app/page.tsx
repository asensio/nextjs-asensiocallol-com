import HomeSlider1 from "./components/HomeSlider/HomeSlider1";
import Expertise from "./components/Expertise/Expertise";
import Resume from "./components/Resume/Resume";
import References from "./components/References/References";
import TrustedBy from "./components/TrustedBy/TrustedBy";
import Footer from "./components/Footer/Footer";

export const metadata = {
  title: 'Abelardo Asensio Callol',
  description: "With 12+ years of professional experience as a software engineer, I believe my qualifications will match your requirements. I have had the opportunity to function in a variety of positions within various teams, making it easy to adapt to new challenges and concepts. I am confident that I can be a valuable asset to your organization while enabling me to develop and grow professionally.Thank you very much for your time and consideration. I look forward to discussing this opportunity with you.",
  openGraph: {
    title: "Abelardo Asensio Callol",
    site_name: "Abelardo Asensio Callol Portfolio",
    url: "https://asensiocallol.com",
    image: "/images/logo/logo.png"
  },
  twitter: {
    title: "Abelardo Asensio Callol",
    card: "With 12+ years of professional experience as a software engineer, I believe my qualifications will match your requirements. I have had the opportunity to function in a variety of positions within various teams, making it easy to adapt to new challenges and concepts. I am confident that I can be a valuable asset to your organization while enabling me to develop and grow professionally.Thank you very much for your time and consideration. I look forward to discussing this opportunity with you.",
  },
  generator: 'Next.js',
  applicationName: 'Abelardo Asensio Callol Portfolio',
  referrer: 'origin-when-cross-origin',
  keywords: ['Sr. Software Engineer', 'FullStack Developer', 'Professional Coder', 'Tech Enthusiast', 'AI Enthusiast'],
  authors: [{name: 'Abelardo Asensio', url: 'https://asensiocallol.com'}],
  colorScheme: 'light',
  creator: 'Abelardo Asensio Callol',
  publisher: 'Abelardo Asensio Callol',
  formatDetection: {
    email: true,
    address: false,
    telephone: true,
  },
}

export default function Page() {
  return (
    <>
      <HomeSlider1/>
      <Expertise/>
      {/*/!*<Portfolio/>*!/*/}
      <Resume/>
      <References/>
      <TrustedBy/>
      <Footer/>
    </>
  );
}
