import "./styles/globals.css";
import "./styles/vendor/aos.css";
import "./styles/vendor/bootstrap.min.css";
import "./styles/vendor/slick.css";
import "./styles/vendor/slick-theme.css";
import {Inter} from "next/font/google";
import Header from "./components/Header/Header";
import PopUpMobileMenu from "./components/PopUpMobileMenu/PopUpMobileMenu";
import Script from "next/script";

const inter = Inter({subsets: ["latin"]});
export const metadata = {
  title: {
    template: '%s | Abelardo Asensio Callol',
  },
}
export default function RootLayout({children,}: { children: React.ReactNode; }) {
  return (
    <html
      lang="en"
      className=" sizes customelements history pointerevents postmessage webgl websockets cssanimations csscolumns csscolumns-width csscolumns-span csscolumns-fill csscolumns-gap csscolumns-rule csscolumns-rulecolor csscolumns-rulestyle csscolumns-rulewidth csscolumns-breakbefore csscolumns-breakafter csscolumns-breakinside flexbox picture srcset webworkers"
    >
    <body
      className="template-color-1 spybody white-version"
      data-spy="scroll"
      data-target=".navbar-example2"
      data-offset="150"
      data-aos-easing="ease"
      data-aos-duration="400"
      data-aos-delay="0"
    >
    <Header/>
    <PopUpMobileMenu/>
    <main className="main-page-wrapper">{children}</main>
    <Script
      src="https://www.googletagmanager.com/gtag/js?id=G-R8VJ2M1JR3"
      strategy="afterInteractive"
    />
    <Script id="google-analytics" strategy="afterInteractive">
      {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'G-R8VJ2M1JR3');
        `}
    </Script>
    </body>
    </html>
  );
}
